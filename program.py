import argparse
import random


def fill_array_rand(size):
    arr = []
    for i in range(size):
        arr.append(random.randrange(100))
    return arr


def print_array(array):
    print(array)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--size", required=True)
    parser.add_argument("--output", default='result.txt')
    args = parser.parse_args()
    with open(args.output, "w") as output:
        if args.size and int(args.size) > 0:
            arr = fill_array_rand(size=int(args.size))
            print_array(arr)
            output.write(str(arr))
        else:
            print("Enter correct size of array")
            output.write("Enter correct size of array")
