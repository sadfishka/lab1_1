**Лабораторная работа №1 - Грынь Лилиана БСБО-05-19**

|*Часть 1*|

**Описание выполненного функционала**

* Приложение выполняет две функции, первая функция заполняет массив случайными значениями, вторая функция выводит массив на экран
* Приложение написано на яп python
* Средствами CI производится сборка проекта, конфигурация указана в файле .gitlab-ci.yml.
* Входные данные задаются в это же файле для работы program.py: SIZE, OUTPUT

**Пример работы приложения при SIZE = 20**
![img.png](img.png)
